import hello_pb2_grpc as pb2
import hello_pb2
from concurrent import futures
import grpc
from grpc import ServerInterceptor
import ssl

class Register(pb2.IntroducationServicer):
    def Register(self, request, context):
        print(request)
        res = hello_pb2.RegisterOutput()
        res.Response = "successfull"
        return res

# SSL/TLS
def main():
    with open('certs/server.key', 'rb') as f:
        private_key = f.read()
    with open('certs/server.crt', 'rb') as f:
        certificate_chain = f.read()

    server_cert = 'certs/server.crt'
    server_key = 'certs/server.key'
    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain(certfile=server_cert, keyfile=server_key)
    ssl_context.set_ciphers('ECDHE-RSA-AES256-SHA384')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_2

    print("starting server on 50051")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    pb2.add_IntroducationServicer_to_server(Register(), server)
    server_creds = grpc.ssl_server_credentials([(private_key, certificate_chain)], root_certificates=None, require_client_auth=False)
    server.add_secure_port("[::]:50051",server_creds)
    server.start()
    server.wait_for_termination()

if __name__ == "__main__":
    main()