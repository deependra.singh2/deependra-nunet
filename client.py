import grpc
import hello_pb2
import hello_pb2_grpc

def test_grpc():
    print("Running test script")
    with open ('server.crt', 'rb') as f:
        server_cert = f.read()
    client_creds = grpc.ssl_channel_credentials(root_certificates=server_cert)
    with grpc.secure_channel("localhost:50051", client_creds) as channel:
        stub = hello_pb2_grpc.IntroducationStub(channel)
        response = stub.Register(hello_pb2.RegisterInput(Name="Vishal Patidar", Age="23", City="Indore", msg="Hello!"))
        print(f"Response : {response}")

if __name__ == "__main__":
    test_grpc()